MIKA-PAGETURNER
===============


- A wireless footpedal for scrolling documents and turning pages
- Uses Seeed Studio XIAO nRF52840 board to create a wireless (BLE) keyboard (HID, Human Interface Device)
- 2 buttons scroll and/or turn pages
- 1 button changes mode, up/down arrows or pageUp/pageDown (maybe also left/righr arrows)
- 10 ms debounce circuits for the three buttons
- can be used with a LiPo battery
- firmware programmed using CircuitPython
- indication lights, green OK, red ERROR, two blue ones for status


PCB (in directory `v1/kicad/`)
---------------------------


[PCB Schematic](v1/kicad/doc/mika-pageturner-schematic.pdf)

[Bill of Materials (BOM) / Part List](v1/kicad/doc/mika-pageturner-bom.csv)

[Digi-Key Shopping Cart, 1 PCB](https://www.digikey.fi/short/z4c0dt9t)

[Case/Box Lid Drills](v1/kicad/doc/mika-pageturner-boxdrill.pdf)

![](v1/kicad/doc/mika-pageturner-3D-front.png)

![](v1/kicad/doc/mika-pageturner-3D-back.png)


Links
-----

https://wiki.seeedstudio.com/XIAO_BLE/

https://learn.adafruit.com/ble-hid-keyboard-buttons-with-circuitpython


Available Pins
--------------

Adafruit CircuitPython 8.0.0-beta.5 Seeed XIAO nRF52840

microcontroller.pin.P0_00	 
microcontroller.pin.P0_01	 
microcontroller.pin.P0_02	 board.A0 board.D0 
microcontroller.pin.P0_03	 board.A1 board.D1 
microcontroller.pin.P0_04	 board.A4 board.D4 board.SDA 
microcontroller.pin.P0_05	 board.A5 board.D5 board.SCL 
microcontroller.pin.P0_06	 board.LED_BLUE 
microcontroller.pin.P0_07	 board.IMU_SDA 
microcontroller.pin.P0_08	 
microcontroller.pin.P0_09	 board.NFC1 
microcontroller.pin.P0_10	 board.NFC2 
microcontroller.pin.P0_11	 board.IMU_INT1 
microcontroller.pin.P0_12	 
microcontroller.pin.P0_13	 
microcontroller.pin.P0_14	 board.READ_BATT_ENABLE 
microcontroller.pin.P0_15	 
microcontroller.pin.P0_16	 board.PDM_DATA 
microcontroller.pin.P0_17	 board.CHARGE_STATUS 
microcontroller.pin.P0_18	 
microcontroller.pin.P0_19	 
microcontroller.pin.P0_20	 
microcontroller.pin.P0_21	 
microcontroller.pin.P0_22	 
microcontroller.pin.P0_23	 
microcontroller.pin.P0_24	 
microcontroller.pin.P0_25	 
microcontroller.pin.P0_26	 board.LED board.LED_RED 
microcontroller.pin.P0_27	 board.IMU_SCL 
microcontroller.pin.P0_28	 board.A2 board.D2 
microcontroller.pin.P0_29	 board.A3 board.D3 
microcontroller.pin.P0_30	 board.LED_GREEN 
microcontroller.pin.P0_31	 board.VBATT 
microcontroller.pin.P1_00	 board.PDM_CLK 
microcontroller.pin.P1_01	 
microcontroller.pin.P1_02	 
microcontroller.pin.P1_03	 
microcontroller.pin.P1_04	 
microcontroller.pin.P1_05	 
microcontroller.pin.P1_06	 
microcontroller.pin.P1_07	 
microcontroller.pin.P1_08	 board.IMU_PWR 
microcontroller.pin.P1_09	 
microcontroller.pin.P1_10	 board.MIC_PWR 
microcontroller.pin.P1_11	 board.D6 board.TX 
microcontroller.pin.P1_12	 board.D7 board.RX 
microcontroller.pin.P1_13	 board.D8 board.SCK 
microcontroller.pin.P1_14	 board.D9 board.MISO 
microcontroller.pin.P1_15	 board.D10 board.MOSI 

List created with https://github.com/todbot/circuitpython-tricks#show-microcontrollerpin-to-board-mappings
