import microcontroller
import board
import time
import keypad # key press events
import digitalio # for LEDs

import adafruit_ble
from adafruit_ble.services.standard.hid import HIDService
from adafruit_ble.services.standard.device_info import DeviceInfoService
from adafruit_ble.advertising.standard import ProvideServicesAdvertisement
from adafruit_ble.advertising import Advertisement

from adafruit_hid.keyboard import Keyboard

DEBUG_MODE = False # True/False

""" https://usb.org/sites/default/files/hut1_21_0.pdf#page=83 """
RIGHT_ARROW = 0x4F
LEFT_ARROW = 0x50
DOWN_ARROW = 0x51
UP_ARROW = 0x52
PAGE_DOWN = 0x4E
PAGE_UP = 0x4B

OK_LED_PIN = board.D7
ERROR_LED_PIN = board.D8
STATUS1_LED_PIN = board.D9
STATUS2_LED_PIN = board.D10

OK = digitalio.DigitalInOut(OK_LED_PIN)
ERROR = digitalio.DigitalInOut(ERROR_LED_PIN)
STATUS1 = digitalio.DigitalInOut(STATUS1_LED_PIN)
STATUS2 = digitalio.DigitalInOut(STATUS2_LED_PIN)
OK.direction = digitalio.Direction.OUTPUT
ERROR.direction = digitalio.Direction.OUTPUT
STATUS1.direction = digitalio.Direction.OUTPUT
STATUS2.direction = digitalio.Direction.OUTPUT

OK.value = True
time.sleep(0.1)
ERROR.value = True
time.sleep(0.1)
STATUS2.value = True
time.sleep(0.1)
STATUS1.value = True
time.sleep(1)

OK.value = False
time.sleep(0.1)
ERROR.value = False
time.sleep(0.1)
STATUS2.value = False
time.sleep(0.1)
STATUS1.value = False

LEFT_BUTTON_PIN = board.D0
RIGHT_BUTTON_PIN = board.D6
MIDDLE_BUTTON_PIN = board.D5
BUTTON_PINS = (LEFT_BUTTON_PIN, RIGHT_BUTTON_PIN, MIDDLE_BUTTON_PIN) # tuple

# LOOK-UP TABLE FOR KEY PRESS ACTIONS IN DIFFERENT MODES
KEY_MAPPING = {"ARROWS": [UP_ARROW, DOWN_ARROW],
               "ARROWS_LR": [LEFT_ARROW, RIGHT_ARROW],
               "PAGES": [PAGE_UP, PAGE_DOWN]}
MODE = "ARROWS"
print(KEY_MAPPING[MODE])
#print(KEY_MAPPING["ARROWS"][0])
#print(KEY_MAPPING["ARROWS"][1])
#print(KEY_MAPPING["PAGES"])

keys = keypad.Keys(
  BUTTON_PINS,
  value_when_pressed=False,
  pull=False,
  interval=0.1 # 100ms debounce
)

if DEBUG_MODE:
    print("SHOW microcontroller.pin TO board MAPPINGS")
    for pin in dir(microcontroller.pin):
        if isinstance(getattr(microcontroller.pin, pin), microcontroller.Pin):
            print("".join(("microcontroller.pin.", pin, "\t")), end=" ")
            for alias in dir(board):
                if getattr(board, alias) is getattr(microcontroller.pin, pin):
                    print("".join(("", "board.", alias)), end=" ")
        print()

# BLUETOOTH
hid = HIDService()
device_info = DeviceInfoService(software_revision=adafruit_ble.__version__,
                                manufacturer="Mika")
advertisement = ProvideServicesAdvertisement(hid)
advertisement.appearance = 961
scan_response = Advertisement()
scan_response.complete_name = "MIKA-PAGETURNER"
ble = adafruit_ble.BLERadio()
ble.name = "MIKA-PAGETURNER"

if not ble.connected:
    print("advertising")
    ble.start_advertising(advertisement, scan_response)
else:
    print("already connected")
    print(ble.connections)

k = Keyboard(hid.devices)

led_timer_start = time.monotonic()
mode_change_time = led_timer_start

while True:
    
    while not ble.connected:
        pass
        print("not connected...")
        ERROR.value = True
    ERROR.value = False
    
    print("CONNECTED")
    print("Start typing:")
    
    while ble.connected:
        
        # SET/RESET LEDS
        # LEDS OFF AFTER 10 SECONDS
        if time.monotonic() < (led_timer_start + 10):
            OK.value = True
        else:
            OK.value = False
        if time.monotonic() < mode_change_time + 10:
            if MODE == "ARROWS":
                STATUS1.value = True
                STATUS2.value = False
            elif MODE == "ARROWS_LR":
                STATUS1.value = False
                STATUS2.value = True
            elif MODE == "PAGES":
                STATUS1.value = True
                STATUS2.value = True
        else:
            STATUS1.value = False
            STATUS2.value = False
        
        # CATCH KEY PRESS
        key_event = keys.events.get()
        if key_event:
            key_number = key_event.key_number
            #print(key_number)
            if key_event.pressed:
                if key_number == 1:
                    k.send(KEY_MAPPING[MODE][1])
                    print("DOWN " + str(KEY_MAPPING[MODE][1]))
                if key_number == 0:
                    k.send(KEY_MAPPING[MODE][0])
                    print("UP")
                if key_number == 2:
                    # CHANGE MODE
                    if MODE == "ARROWS":
                        MODE = "ARROWS_LR"
                    elif MODE == "ARROWS_LR":
                        MODE = "ARROWS"
                    #elif MODE == "PAGES":
                        #MODE = "ARROWS"
                    mode_change_time = time.monotonic()
                    print("MIDDLE, changed mode to " + MODE)

    ble.start_advertising(advertisement)
    
